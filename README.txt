1.- Para construir la imagen:

        docker build -t friendlyhello .
 

2.- Ahora ejecutamos nuestra imagen:

        docker run --rm -p 4000:80 friendlyhello


3.- Arrancamos un conjunto de 5 contenedores de Docker

        docker-composer up -d --scale web=5


4.- Para ver que funciona correctamente, accedemos en google a:

        http://localhost:4000/


5.- Detenemos y borramos los contenedores anteriormente iniciados:

        docker-compose down
        
        

